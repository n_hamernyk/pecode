import React, {useEffect, useState, useRef} from 'react';
import {getEpisodes} from '../../Services/Api';
import EpisodesFilter from './EpisodesFilter/EpisodesFilter'
import TableComponent from '../Table/TableComponent';


const Episodes = () =>{
    const [episodes, setEpisodes] = useState([]);
    const [totalCount, setTotalCount] = useState(1);
    const [currentPage, setcurrentPage] = useState(1);
    const [nameFilterValue, setNameFilterValue] = useState('')
   
    useEffect(()=>{
        const queryParams = {
            page: currentPage,
        };

        if(nameFilterValue) queryParams.name = nameFilterValue;

        const searchParams = new URLSearchParams(queryParams);

        const timedOut = setTimeout(() => getData('?'+ searchParams.toString()), 200);

        return () => clearTimeout(timedOut);
    },[nameFilterValue, currentPage]);

   
    const getData = async(params) =>{
        try {
            const response = await getEpisodes(params);
            setEpisodes(response.results);
            setTotalCount(response.info.count)
        } catch(e) {
            console.log(e);
            setTotalCount(0)
        }
    }

    const handleNameFilter = (e) => {
        setNameFilterValue(e.target.value)
    }

    const headers = ['ID', 'Name', 'Air Date', 'Episode', 'Created'];
      
    return (
        <>
            <EpisodesFilter 
                nameFilterValue={nameFilterValue}
                handleNameFilter={handleNameFilter}
            />
            <TableComponent
                headers={headers}
                rows={episodes}
                totalCount={totalCount}
                currentPage={currentPage}
                setcurrentPage={setcurrentPage}          
            />
        </>
      );
    }
    
    
export default Episodes;