import React from 'react';
import TextField from '@material-ui/core/TextField';
import classes from './EpisodesFilter.module.css';

const EpisodesFilter = ({nameFilterValue, handleNameFilter}) =>{

    return(
        <div className={classes.EposodeFilter}>
            <TextField
                className={classes.SearchFilter} 
                label="Search by Name"
                type="search"
                value={nameFilterValue}
                variant="outlined"
                placeholder="Enter the Name"
                onChange={(e)=>handleNameFilter(e)}
            />
        </div>
    )
}

export default EpisodesFilter;