import React from 'react';
import classes from'./TableComponent.module.css';
import {withStyles, Table, TableBody, 
    TableCell, TableContainer, 
    TableHead, TableRow, Paper, TablePagination} from '@material-ui/core';
import moment from "moment";


const StyledTableCell = withStyles((theme) => ({
    head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    fontSize: 18
    },
    body: {
    fontSize: 16,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    },
}))(TableRow);

const TableComponent = ({headers, rows, totalCount, currentPage, setcurrentPage, locations}) =>{
    return(
        <>
            {rows ? (
                <TableContainer 
                    component={Paper}
                    className={classes.Table}
                >
                    <Table>
                    <TableHead>
                        <TableRow>
                            {   
                                headers.map((header, index)=>
                                    <StyledTableCell
                                        key={index}
                                        align={index===headers.length-1? 'right': 'left'}
                                    > 
                                        {header} 
                                    </StyledTableCell>
                                )          
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {   
                            rows  &&
                            rows.map((row) => (
                                <StyledTableRow key={row.id}>
                                    <StyledTableCell> {row.id} </StyledTableCell>
                                    <StyledTableCell> {row.name} </StyledTableCell>
                                    <StyledTableCell> {locations? row.type : row.air_date} </StyledTableCell>
                                    <StyledTableCell> {locations? row.dimension : row.episode} </StyledTableCell>
                                    <StyledTableCell align="right"> {moment(row.created).format("Do MMMM YYYY")} </StyledTableCell>
                                </StyledTableRow>
                            ))
                        }
                    </TableBody>
                    <TablePagination 
                        rowsPerPageOptions={[]}
                        count={totalCount} 
                        page={currentPage-1} 
                        rowsPerPage={20}
                        onChangePage={(e,v) => setcurrentPage(v+1)} 
                    />
                    </Table>
                </TableContainer>
              ) : (
                <h1 className={classes.Message}> There is no results :( </h1>
            )}
        </>
      );    
    }


export default TableComponent;