import React from 'react';
import TextField from '@material-ui/core/TextField';
import classes from './LocationsFilter.module.css';

const EpisodesFilter = ({nameFilterValue, handleNameFilter, typeFilterValue, 
                         handleTypeFilter, dimensionFilterValue, handleDimensionFilter}) =>{

    return(
        <div className={classes.FiltersPanel}>
            <TextField
                className={classes.FilterItem}
                label="Search by Name"
                type="search"
                value={nameFilterValue}
                variant="outlined"
                placeholder="Enter the Name"
                onChange={(e)=> handleNameFilter(e) }
            />
            <TextField
                className={classes.FilterItem}
                label="Search by Type"
                type="search"
                value={typeFilterValue}
                variant="outlined"
                placeholder="Enter the Type"
                onChange={(e)=>handleTypeFilter(e)}
            />
            <TextField
                className={classes.FilterItem}
                label="Search by Dimension"
                type="search"
                value={dimensionFilterValue}
                variant="outlined"
                placeholder="Enter the Dimension"
                onChange={(e)=>handleDimensionFilter(e)}
            />
        </div>
    )
}

export default EpisodesFilter;