import React, {useEffect, useState} from 'react';
import {getLocations} from '../../Services/Api';
import LocationFilter from './LocationsFilter/LocationsFilter'
import TableComponent from '../Table/TableComponent'


const Locations = () =>{
    const [locations, setLocations] = useState([]);
    const [totalCount, setTotalCount] = useState(1);
    const [currentPage, setcurrentPage] = useState(1);
    const [nameFilterValue, setNameFilterValue] = useState('')
    const [typeFilterValue, setTypeFilterValue] = useState('')
    const [dimensionFilterValue, setDimensionFilterValue] = useState('')

    useEffect(()=>{
        const queryParams = {
            page: currentPage,
        };
        
        if(nameFilterValue) queryParams.name = nameFilterValue;
        if(typeFilterValue) queryParams.type = typeFilterValue;
        if(dimensionFilterValue) queryParams.dimension = dimensionFilterValue;
    
        const searchParams = new URLSearchParams(queryParams);
        
        const timedOut = setTimeout(() => getData('?'+ searchParams.toString()), 200);
        return () => clearTimeout(timedOut);
        
    },[nameFilterValue, typeFilterValue, dimensionFilterValue, currentPage])


    const getData = async(params) =>{
        try {
            const response = await getLocations(params);
            setLocations(response.results);
            setTotalCount(response.info.count)
        } catch(e) {
            console.log(e);
            setTotalCount(0)
        }
    }

    const handleNameFilter = (e) =>{
        setNameFilterValue(e.target.value)
        setcurrentPage(1)
    }
  
    const handleTypeFilter = (e) =>{
        setTypeFilterValue(e.target.value);
        setcurrentPage(1)
    }

    const handleDimensionFilter = (e) =>{
        setDimensionFilterValue(e.target.value)
        setcurrentPage(1)
    }

    const headers = ['ID', 'Name', 'Type', 'Dimension', 'Created'];

        return(
            <>
                <LocationFilter 
                    nameFilterValue={nameFilterValue}
                    handleNameFilter={handleNameFilter}
                    typeFilterValue={typeFilterValue}
                    handleTypeFilter={handleTypeFilter}
                    dimensionFilterValue={dimensionFilterValue}
                    handleDimensionFilter={handleDimensionFilter}
                />
                <TableComponent
                    headers={headers}
                    rows={locations}
                    totalCount={totalCount}
                    currentPage={currentPage}
                    setcurrentPage={setcurrentPage}
                    locations={true}
                />
            </>
        );    
    }

export default Locations;