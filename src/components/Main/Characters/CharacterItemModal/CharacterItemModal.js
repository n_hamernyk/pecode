import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Modal, Backdrop, Fade} from '@material-ui/core';
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const CharacterItemModal = ({selectedCharacter, modalActive, handleCloseModal}) =>{
    const classes = useStyles();

    return (
        <Modal
          className={classes.modal}
          open={modalActive}
          onClose={handleCloseModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={modalActive}>
            <div className={classes.paper}>
              <h1>Name: {selectedCharacter.name}</h1>
              <h3>Status: {selectedCharacter.status}</h3>
              <h3>Species: {selectedCharacter.species}</h3>
              <h3>Gender: {selectedCharacter.gender}</h3>
              <h3>Location: {selectedCharacter.location.name}</h3>
              <h4>Created: {moment(selectedCharacter.created).format("Do MMMM YYYY")} </h4>
            </div>
          </Fade>
        </Modal>
    );
  }

export default CharacterItemModal;