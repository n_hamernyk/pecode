import React, {useEffect, useState} from 'react';
import classes from './characters.module.css';
import CharactersFilter from './CharactersFilter/CharactersFilter'
import CharacterItemModal from './CharacterItemModal/CharacterItemModal'
import {getCharecters} from '../../Services/Api';
import { Pagination } from '@material-ui/lab';
import Spinner from '../../UI/Spinner';

const Characters = () =>{
    const [characters, setCharacters] = useState([]);
    const [currentPage, setcurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const [speciesValue, setSpeciesValue] = useState('');
    const [statusValue, setStatusValue] = useState('');
    const [genderValue, setGenderValue] = useState('');
    const [selectedCharacter, setSelectedCharacter] = useState(null);
    const [modalIsActive, setModalIsActive] = useState(false); 
    const [loading, setLoading] = useState(false);
    
    useEffect(() => {
        const queryParams = {
            page: currentPage,
        };
        
        if(speciesValue) queryParams.species = speciesValue;
        if(statusValue) queryParams.status = statusValue;
        if(genderValue) queryParams.gender = genderValue;
          
        const searchParams = new URLSearchParams(queryParams);

        getData('?'+ searchParams.toString());
    },[speciesValue, statusValue, genderValue, currentPage]);

    const getData = async(params) =>{
        setLoading(true);
        try {
            const response = await getCharecters(params);
            setCharacters(response.results ?? []);
            setTotalPages(response.info.pages);
        } catch(e) {
            console.log(e);
        } finally{
            setLoading(false);
        }
    }

    const handleSpeciesFilter = (value) =>{
        setSpeciesValue(value)
        setcurrentPage(1)
    }

    const handleStatusFilter = (value) =>{
        setStatusValue(value)
        setcurrentPage(1)
    }

    const handleGenderFilter = (value) =>{
        setGenderValue(value)
        setcurrentPage(1)
    }

    const handleCharacterClick = (id) =>{
        let findItem = characters.find(findItem => findItem.id === id);
        setSelectedCharacter(findItem)
        setModalIsActive(true)
    }


    const characterList = (
        <div className={classes.CharactersList}>
                {
                    characters.length ? (
                    characters.map((character)=>
                        <article 
                            key={character.id}
                            className={classes.CharacterItem}
                            onClick={()=>handleCharacterClick(character.id)}
                        >
                            <img
                                className={classes.CharacterItemImage}
                                src={character.image}
                                alt={character.name}
                            />
                            <div className={classes.CharacterItemDescription}>
                                <h1 className={classes.CharacterItemTitle}>{character.name}</h1>
                                <div className={classes.CharacterItemStatus}>
                                    <span
                                        className={character.status==='Alive' ?  [classes.CharacterStatusIcon , classes.StatusAlive].join(' ')
                                        :character.status==='Dead' ? [classes.CharacterStatusIcon, classes.StatusDead].join(' ')
                                        :character.status==='unknown' ? [classes.CharacterStatusIcon, classes.StatusUnknown].join(' ')
                                        :'characterStatusIcon'
                                        } 
                                    />
                                    <span className={classes.CharacterItemStatusText} >{character.status} - {character.species}</span>
                                </div>
                                <p className={classes.CharacterItemLabel}> Gender: </p>
                                <p className={classes.CharacterItemText}> {character.gender} </p>
                                <p className={classes.CharacterItemLabel}> Last known location: </p>
                                <p className={classes.CharacterItemText}> {character.location.name} </p>
                            </div>
                        </article>                
                )
                    )
                : <h1 className={classes.CharacterMessage}> There is not characters with your parameters :( </h1>
                }
            </div>
    );

    return(
        <div className={classes.Characters}>
            <CharactersFilter
                handleSpeciesFilter={handleSpeciesFilter}
                speciesValue={speciesValue}
                handleStatusFilter={handleStatusFilter}
                statusValue={statusValue} 
                handleGenderFilter={handleGenderFilter}
                genderValue={genderValue}   

            />
            {loading ?  <Spinner/>  :characterList}
            <Pagination 
                className={classes.CharacterPagination}
                count={totalPages} 
                page={currentPage} 
                onChange={(e,v) => setcurrentPage(v)} 
                size='large'
                color='primary'
            />
            {selectedCharacter &&
            <CharacterItemModal 
                selectedCharacter={selectedCharacter}  
                modalActive={modalIsActive}
                handleCloseModal={() => setModalIsActive(false)}          
            />
         }
        </div>
    )
}

export default Characters;



