import React from 'react';
import './CharactersFilter.css'
import {InputLabel, MenuItem, FormControl, Select} from '@material-ui/core';


const CharactersFilter = ({speciesValue,
                            statusValue, 
                            genderValue,
                            handleSpeciesFilter,
                            handleStatusFilter,
                            handleGenderFilter}) => {

    return(
        <div className="filters">
            <FormControl className="form-control">
                <InputLabel>Species</InputLabel>
                    <Select
                        value={speciesValue}
                        onChange={(event)=>handleSpeciesFilter(event.target.value)}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value='human'>Human</MenuItem>
                        <MenuItem value='alien'>Alien</MenuItem>
                    </Select>
            </FormControl>
            <FormControl className="form-control">
                <InputLabel>Status</InputLabel>
                    <Select
                        value={statusValue}
                        onChange={(event)=>handleStatusFilter(event.target.value)}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value='alive'>Alive</MenuItem>
                        <MenuItem value='dead'>Dead</MenuItem>
                        <MenuItem value='unknown'>unknown</MenuItem>
                    </Select>
            </FormControl>
            <FormControl className="form-control">
                <InputLabel>Gender</InputLabel>
                    <Select
                        value={genderValue}
                        onChange={(event)=>handleGenderFilter(event.target.value)}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value='male'>Male</MenuItem>
                        <MenuItem value='female'>Female</MenuItem>
                        <MenuItem value='genderless'>Genderless</MenuItem>
                        <MenuItem value='unknown'>unknown</MenuItem>
                    </Select>
            </FormControl>
        </div>
    )
}

export default CharactersFilter;