import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {List, ListItem, ListItemIcon , ListItemSecondaryAction, Checkbox, IconButton, ListItemText} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import classes from '../../MyWatchList/MyWatchList.module.css';

const listItemStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      margin: '20px 0',
      height: '60px',
      borderRadius: '25px',
      backgroundColor: '#a4a8a8',
    }
  }));

  const textStyles = makeStyles((theme) => ({
    primary: {
        fontSize:'26px',
        color: 'white'
      }
  }));

const ListItems = ({episodes, deleteItem, handleItemChecked}) =>{
    const listItemClasses = listItemStyles();
    const textClasses = textStyles();
  
    return(
        <>
            <List
                className={classes.ListItems}
            >
                {
                    episodes.map((item) => (
                        <ListItem 
                            key={item.id}
                            onClick={()=>handleItemChecked(item.id)}
                            className={listItemClasses.root}
                            dense 
                            button
                        >
                            <ListItemIcon>
                                <Checkbox
                                    edge="start"
                                    checked={item.checked}
                                    onChange={()=>handleItemChecked(item.id)}
                                    disableRipple
                                />
                            </ListItemIcon>
                            <ListItemText 
                                classes={textClasses}
                                primary={item.name} 
                            />
                            <ListItemSecondaryAction>
                                <IconButton 
                                    aria-label="Delete"
                                    onClick={()=> deleteItem(item.id)}
                                >
                                <DeleteIcon />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    ))
                }               
            </List>
        </>
    )
}

export default ListItems;