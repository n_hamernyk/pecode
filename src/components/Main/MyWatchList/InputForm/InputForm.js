import React, {useState} from "react";
import {makeStyles, Paper, InputBase, Divider, IconButton  } from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles((theme) => ({
    root: {
      padding: "2px 4px",
      display: "flex",
      alignItems: "center",
      width: 400
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1
    },
    iconButton: {
      padding: 10
    },
    divider: {
      height: 28,
      margin: 4
    }
  }));


const InputForm = ({addItem}) =>{
    
    const classes = useStyles();
    const [inputValue, setInputValue] = useState('');


    return(
        <>
            <Paper 
                component="form" 
                className={classes.root}
                onSubmit={event =>{
                    event.preventDefault();
                    addItem(inputValue.trim())
                    setInputValue('')
                }}                 
            >
                <InputBase
                    className={classes.input}
                    placeholder="Add new Episode"
                    value={inputValue}
                    onChange={(e)=>setInputValue(e.target.value)}
                />
                <Divider className={classes.divider} orientation="vertical" />
                <IconButton
                    color="primary"
                    type="submit"
                    className={classes.iconButton}
                >
                    <AddIcon />
                </IconButton>
            </Paper>
        </>
    )
}

export default InputForm;
