import React, { useEffect, useState } from 'react';
import classes from  './MyWatchList.module.css';
import InputForm from './InputForm/InputForm'
import ListItems from './ListItems/ListItems'

const MyWatchList = () =>{
    const [episodes, setEpisodes] = useState([]);

    useEffect(()=>{
        const localStorageData = JSON.parse(localStorage.getItem( 'episodes'));
        setEpisodes(localStorageData)

    },[])

    useEffect(()=>{
        localStorage.setItem('episodes', JSON.stringify(episodes));

    },[episodes])

    const addItem = (name) =>{
        let id = new Date().getTime();

        if(name) {
        setEpisodes(prev => [ ...prev, {
            id,
            name,
            checked: false
        }])};
    }

    const deleteItem = (itemId) => {
        const newEpisodes = episodes.filter((episode) => episode.id !== itemId);
        setEpisodes(newEpisodes)
    }

    const handleItemChecked = (id) =>{
        const newEpisodes = episodes.map(item =>item.id === id ? {...item, checked: !item.checked} : item);
        setEpisodes(newEpisodes);
    }

    return(
        <div
            className={classes.MyWatchList}        
        >
            <InputForm
                addItem={addItem}
            /> 
            <ListItems 
                episodes={episodes}   
                deleteItem={deleteItem}
                handleItemChecked={handleItemChecked}
            />
        </div>
    )
}

export default MyWatchList;