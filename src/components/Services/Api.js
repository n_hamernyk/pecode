export const getCharecters = (params) => {
  return fetch(`https://rickandmortyapi.com/api/character/${params}`)
    .then(res => res.json())
    .then(response => response);
}

export const getEpisodes = (params) => {
  return fetch(`https://rickandmortyapi.com/api/episode/${params}`)
    .then(res => res.json())
    .then(response => response);
}

export const getLocations = (params) => {
  return fetch(`https://rickandmortyapi.com/api/location/${params}`)
    .then(res => res.json())
    .then(response => response);
}

