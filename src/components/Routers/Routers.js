import React from 'react';
import  {Route, HashRouter} from 'react-router-dom';
import Header from '../Header/Header';
import Characters from '../Main/Characters/Characters';
import Episodes from '../Main/Episodes/Episodes';
import Locations from '../Main/Locations/Locations';
import MyWatchList from '../Main/MyWatchList/MyWatchList';
import Main from '../Main/Main';


const Routers = () => {
    return(
        <HashRouter>
            <Header/>
            <Main>
                <Route path='/characters' component={Characters}/>
                <Route path='/episodes' component={Episodes}/>
                <Route path='/locations' component={Locations}/>
                <Route path='/my-watch-list' component={MyWatchList}/>
            </Main>
        </HashRouter>
    )
    
}

export default Routers;