import React from 'react';
import './header.css';
import {Link} from 'react-router-dom';
import {withRouter} from 'react-router-dom'
import {AppBar, Toolbar, Typography, IconButton, Menu, MenuItem} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

const Header = (props) => {

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
      };
    
    const handleClose = () => {
        setAnchorEl(null);
      };
    

    return (
            <AppBar className="app-bar">
                <Toolbar variant="dense">
                    <IconButton 
                        edge="start"  
                        color="inherit" 
                        aria-label="menu" 
                        aria-controls="simple-menu" 
                        aria-haspopup="true" 
                        onClick={handleClick}>
                        <MenuIcon />
                    </IconButton>
                    <Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                        <MenuItem onClick={handleClose}>
                            <Link 
                                to="/characters"
                                className="menu-item"
                            >
                                Characters
                            </Link> 
                        </MenuItem>
                        <MenuItem onClick={handleClose}>
                            <Link 
                                to="/episodes"
                                className="menu-item"
                            >
                                Episodes
                            </Link> 
                        </MenuItem>
                        <MenuItem onClick={handleClose}>
                            <Link 
                                to="/locations"
                                className="menu-item"
                            >
                                Locations
                            </Link> 
                        </MenuItem>
                        <MenuItem onClick={handleClose}>
                            <Link 
                                to="/my-watch-list"
                                className="menu-item"
                            >
                                My watch list
                            </Link> 
                        </MenuItem>
                    </Menu>                    
                    <Typography
                        className="typography"
                        variant="h5" 
                        color="inherit" 
                        align="center"
                    >
                        {props.location.pathname==="/characters"? `Characters`
                        :props.location.pathname==="/episodes"? `Episodes`
                        :props.location.pathname==="/locations"? `Locations`
                        :props.location.pathname==="/my-watch-list"? `My watch list`
                        :null 
                        } 
                    </Typography>
                </Toolbar>
            </AppBar>
    )
}

export default withRouter(Header);